<?php

namespace App\Http\Controllers;

use App\ColdBox;
use App\Temperature;
use Illuminate\Support\Facades\DB;

class ColdBoxController extends Controller
{
    public function index()
    {
        $coldBoxes = ColdBox::all();
        foreach ($coldBoxes as $box) {
            $box->temperature = $this->GetCurrectTemperature($box);
            $box->isError = ($box->temperature == null || $box->temperature < $box->minTemperature || $box->temperature > $box->maxTemperature ? true : false);
        }
        //echo '<pre>' . var_export($coldBoxes, true) . '</pre>';
        return response()->json($coldBoxes);
    }

    public function detail($id)
    {
        return response()->json(ColdBox::find($id));
    }

    public function GetCurrectTemperature(ColdBox $coldBox) {
        $now = date('Y-m-d H:i:s');
        $temperatures = DB::table('temperatures')
            ->where('coldBox_id', $coldBox->id)
            ->whereBetween('created_at', [date('Y-m-d H:i:s', strtotime('-1 minutes')), $now])->get();
        //echo '<pre>' . var_export($temperatures, true) . '</pre>';
        if(count($temperatures) > 0) {
            $sum = 0;
            foreach ($temperatures as $temp) {
                $sum += $temp->value;
            }
            $averageTemp = round($sum / count($temperatures), 1);
            //echo var_dump($temperatures->value);
            return $averageTemp;
        } else {
            return null;
        }
    }
}
