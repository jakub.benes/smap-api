<?php

namespace App\Http\Controllers;

use App\Temperature;
use Illuminate\Http\Request;

class TemperatureController extends Controller
{
    public function create(Request $request)
    {
        $author = Temperature::create($request->all());
        return response()->json($author, 201);
    }
}
